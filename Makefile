rabbitmq:
	docker run -d --hostname my-rabbit --name some-rabbit -p 5672:5672 -p 15672:15672 -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password rabbitmq:management

install:
	(cd client && go get) && (cd server && go get)
.PHONY: rabbitmq install
