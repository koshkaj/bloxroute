# Usage

Definition: Client sends some info to Server by using rabbitmq's message queue. Server obtains it,
prints out to stdout and also logs it to the `server.log` file


## Server
```shell
make install
cd server
go run main.go
```


## Client
```shell
make install
cd client
go run main.go $ARGS
```

`Available Arguments for client call:  GetItem AddItem RemoveItem GetAllItems`

## Examples
```shell
go run main.go AddItem A
go run main.go AddItem B
go run main.go GetAllItems
go run main.go GetItem A
go run main.go RemoveItem B
```

## Screenshot of usage

![example_pic](example.png "Example")