package main

import (
	"bloxroute/server/core"
	"io"
	"log"
	"os"
)

func main() {
	core.Init()
	f, err := os.OpenFile("server.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	wrt := io.MultiWriter(os.Stdout, f)
	log.SetOutput(wrt)
	core.Subscribe()
	defer core.CloseConnection()
}
