package core

import (
	"errors"
	"sync"
)

type Item struct {
	Key string `json:"key"`
}

type Items struct {
	sync.RWMutex
	data []Item
}

func (i *Items) add(item Item) {
	i.Lock()
	defer i.Unlock()
	i.data = append(i.data, item)
}

func (i *Items) getAll() []Item {
	return i.data
}

func (i *Items) get(key string) (*Item, error) {
	i.RWMutex.RLock()
	defer i.RWMutex.RUnlock()
	for _, item := range items.data {
		if item.Key == key {
			return &item, nil
		}
	}
	return nil, errors.New("Item with key: " + key + " Not found")
}

func (i *Items) remove(key string) error {
	i.Lock()
	defer i.Unlock()
	for idx, item := range i.data {
		if item.Key == key {
			i.data = append(i.data[:idx], i.data[idx+1:]...)
			return nil
		}
	}
	return errors.New("Key: " + key + " does not exist")
}
