package core

import (
	"encoding/json"
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
)

type ClientInput struct {
	Data   string
	Action string
}

var conn *amqp.Connection

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func Init() {
	var err error
	conn, err = amqp.Dial("amqp://user:password@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
}

func CloseConnection() {
	conn.Close()
}

func Subscribe() {
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	queue, err := ch.QueueDeclare(
		"bloxroute", // name
		false,       // durable
		false,       // delete when unused
		false,       // exclusive
		false,       // no-wait
		nil,         // arguments
	)
	failOnError(err, "Failed to declare a queue")

	messages, err := ch.Consume(
		queue.Name, // queue
		"",         // core
		true,       // auto-ack
		false,      // exclusive
		false,      // no-local
		false,      // no-wait
		nil,        // args
	)
	failOnError(err, "Failed to register a core")

	var infinite chan struct{}

	go func() {
		var message ClientInput
		for m := range messages {
			err = json.Unmarshal(m.Body, &message)
			failOnError(err, "Unable to unmarshall received data")
			handleMessage(message)
		}
	}()
	<-infinite
}
