package core

import (
	"log"
)

var items = &Items{}

func handleMessage(message ClientInput) {
	if message.Action == "GetItems" {
		log.Println("GET ALL -> ", items.getAll())
	} else if message.Action == "GetItem" {
		item, err := items.get(message.Data)
		if err != nil {
			log.Println(err)
			return
		}
		log.Println("GET -> ", item.Key)
	} else if message.Action == "RemoveItem" {
		err := items.remove(message.Data)
		if err != nil {
			log.Println(err)
			return
		}
		log.Println("REMOVED -> ", message.Data)
	} else if message.Action == "AddItem" {
		var item = &Item{Key: message.Data}
		items.add(*item)
		log.Println("ADDED ->", item.Key)
	}

}
