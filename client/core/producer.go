package core

import (
	"context"
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
)

var conn *amqp.Connection

func failOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func Init() {
	var err error
	conn, err = amqp.Dial("amqp://user:password@localhost:5672/")
	failOnError(err, "Failed to connect to RabbitMQ")
}

func CloseConnection() {
	conn.Close()
}

func Publish(data []byte) {
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()

	payload := amqp.Publishing{
		DeliveryMode: amqp.Persistent,
		ContentType:  "application/json",
		Body:         data,
	}
	queue, err := ch.QueueDeclare(
		"bloxroute", // name
		false,       // durable
		false,       // delete when unused
		false,       // exclusive
		false,       // no-wait
		nil,         // arguments
	)
	failOnError(err, "Failed to declare a queue")

	err = ch.PublishWithContext(context.Background(),
		"",         // exchange
		queue.Name, // routing key
		false,      // mandatory
		false,      // immediate
		payload,
	)
	failOnError(err, "Failed to publish a message")
}
