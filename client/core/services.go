package core

import (
	"encoding/json"
	"log"
)

type ClientInput struct {
	Data   string
	Action string
}

func handleError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func AddItem(key string) {
	client_input := &ClientInput{Data: key, Action: "AddItem"}

	data, err := json.Marshal(client_input)
	handleError(err)
	Publish(data)
}

func RemoveItem(key string) {
	client_input := &ClientInput{Data: key, Action: "RemoveItem"}

	data, err := json.Marshal(client_input)
	handleError(err)
	Publish(data)
}

func GetItem(key string) {
	client_input := &ClientInput{Data: key, Action: "GetItem"}

	data, err := json.Marshal(client_input)
	handleError(err)
	Publish(data)
}

func GetItems() {
	client_input := &ClientInput{Action: "GetItems"}

	data, err := json.Marshal(client_input)
	handleError(err)
	Publish(data)
}
