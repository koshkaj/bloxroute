package main

import (
	"client/core"
	"log"
	"os"
)

func main() {
	core.Init()
	defer core.CloseConnection()
	args := os.Args[1:]
	action := args[0]
	switch action {
	case "AddItem":
		core.AddItem(args[1])
	case "RemoveItem":
		core.RemoveItem(args[1])
	case "GetItem":
		core.GetItem(args[1])
	case "GetAllItems":
		core.GetItems()
	default:
		log.Fatalf("Please input one of the following -> AddItem, RemoveItem, GetItem, GetAllItems\n")
	}
}
